# Step by Step Commands

```
terraform apply
aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)
helm repo add confluentinc https://packages.confluent.io/helm
helm repo update
kubectl create namespace confluent
kubectl config set-context --current --namespace confluent
helm upgrade --install confluent-operator confluentinc/confluent-for-kubernetes
kubectl apply -f https://raw.githubusercontent.com/confluentinc/confluent-kubernetes-examples/master/quickstart-deploy/confluent-platform.yaml [if not local]
kubectl apply -f ~/GITprojects/confluent-kubernetes-examples/quickstart-deploy/confluent-platform.yaml
kubectl apply -f https://raw.githubusercontent.com/confluentinc/confluent-kubernetes-examples/master/quickstart-deploy/producer-app-data.yaml
kubectl port-forward controlcenter-0 9021:9021
```

# Rest Proxy API
```
# Get topics
curl --silent -X GET http://localhost:8082/topics/ | jq

# Get Clusters
curl --silent -X GET http://localhost:8082/v3/clusters/ | jq

# Create Topics
curl --silent -X POST -H "Content-Type: application/json" --data '{"topic_name": "test-topic-1"}' http://localhost:8082/v3/clusters/<cluster-id>/topics | jq

```